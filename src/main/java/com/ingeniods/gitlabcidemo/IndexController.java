package com.ingeniods.gitlabcidemo;

import java.time.LocalDateTime;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class IndexController {
        @GetMapping("/")
    @ResponseBody
    public String index() {
        return "Hola mundo " + LocalDateTime.now();
    }
}

